import 'dart:io';

class Human {

  String? name = stdin.readLineSync();

  void walk() {
    print('$name is Human can Walk');
  }

  void eat() {
    print('$name is Human can Eat');
  }

  void run() {
    print('$name is Human can Run');
  }

  void speak() {
    print('$name is Human can Speak');
  }
}


class Male extends Human {
  void testosterone() {
    print('Testosterone is the main sex hormones in Male.');
  }  
}

class Female extends Human {
  void estrogen() {
    print('Estrogen is the main sex hormones in Female.');
  }
}