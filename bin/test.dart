import 'dart:io';
import 'inheritance.dart';

void main() {

  Male m = new Male();
  m.walk();
  m.eat();
  m.run();
  m.speak();
  m.testosterone();

  print("*************************************************************");

  Female f = new Female();
  f.walk();
  f.eat();
  f.run();
  f.speak();
  f.estrogen();

  print("*************************************************************");

}